class RPNCalculator
  attr_accessor :numbers

  def initialize
    @numbers = []
  end

  def push(n)
    @numbers.push(n)
  end

  def plus
    @numbers.length > 1 ? @numbers.push(@numbers.pop(2).inject{|a,b| a+b}  ) : fail
  end

  def minus
    @numbers.length > 1 ? @numbers.push(@numbers.pop(2).inject{|a,b| a-b} ) : fail
  end

  def divide
    @numbers.length > 1 ? @numbers.push(@numbers.pop(2).inject{|x,y| x*1.0 / y} ) : fail
  end

  def times
    @numbers.length > 1 ? @numbers.push(@numbers.pop(2).inject{|a,b| a*b} ) : fail
  end

  def value
    @value = @numbers[-1]
  end

  def tokens(str)
    str.split(' ').map{|n| n.to_i.to_s == n ? n.to_i : n.to_sym}
  end

  def evaluate(str)
    @numbers = []
    opps = {:+ => Proc.new {plus}, :- => Proc.new{minus}, :/ => Proc.new{divide}, :* => Proc.new{times} } # method in proc, thank you Frank :D
    tokens(str).chunk{|n| n.is_a?(Integer)}.each{|e,a| e == true ? a.each{|a| push(a) } : a.each {|o| (opps[o].call) }}
    @numbers[0]
  end

  def fail
    begin
      raise "calculator is empty"
    end
  end

end
